class Car {
    constructor(scene) {
        //creating the Sphere
        this._scene = scene;
        const mesh = BABYLON.Mesh.CreateSphere('sphere1', 3, 3, scene);
        mesh.position.y = 10;
        mesh.material = new BABYLON.StandardMaterial('texture1', scene);
        mesh.material.emissiveColor = new BABYLON.Color3(1, 0, 0);
        this.mesh = mesh;

        //creating the target that the camera is following
        const target = BABYLON.Mesh.CreateBox('box1', 0.1, scene);
        target.position = this.mesh.position;
        this.target = target;
    }

    //function for the movement sphere
    updateMovement(keys, camera) {
        //vector where the Camera is "looking at" (eventually using the normalized direction-vector for consistent velocities)
        var rawDirection = new BABYLON.Vector3((this.mesh.position.x - camera.cam.position.x), 0, (this.mesh.position.z - camera.cam.position.z));
        var normalizedDirection = rawDirection.normalize();
        var normalizedDirectionBack = new BABYLON.Vector3(-normalizedDirection.x, -normalizedDirection.y, -normalizedDirection.z);
        var direction = new BABYLON.Vector3(0, 0, 0);

        //actual movement of the sphere, the steering is realized through the rotation of the camera
        //moving fowards
        if (keys[38]) {
            direction = normalizedDirection;
            this.direction = direction;
        }
        //moving backwards
        if (keys[40]) {
            direction = normalizedDirectionBack;
            this.direction = direction;
        }

        //editing the velocity and applying the impulse to the sphere
        var f = 4;
        var finalDirection = new BABYLON.Vector3(direction.x / f, direction.y / f, direction.z / f)
        this.mesh.physicsImpostor.applyImpulse(finalDirection, this.mesh.getAbsolutePosition());
        this.target.position = this.mesh.position;
    }

    //function where the physics of the car are applied
    applyPhysics() {
        this.mesh.physicsImpostor = new BABYLON.PhysicsImpostor(this.mesh, BABYLON.PhysicsImpostor.SphereImpostor, {
            mass: 1,
            restitution: 0,
            friction: 2,
        }, this._scene);
    }
}
