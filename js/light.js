class Light {
  constructor(scene){
    var light = new BABYLON.SpotLight('light1', new BABYLON.Vector3(0, 30, 0), new BABYLON.Vector3(0, -1, 0), 0.8, 5, scene);
    light.diffuse = new BABYLON.Color3(0, 1, 1);
    light.specular = new BABYLON.Color3(1, 1, 1);

    this.light = light;
  }

  updateLight(target){
    this.light.position = new BABYLON.Vector3(target.x, 30, target.z);
    this.light.direction = new BABYLON.Vector3(target.x, -1000, target.z);
  }
}
