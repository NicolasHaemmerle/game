class Camera {
    constructor(scene) {
        var camera = new BABYLON.ArcRotateCamera('cam', 0, 1, 100, new BABYLON.Vector3.Zero(), scene);
        scene.activeCamera = camera;
        this.cam = camera;
    }

    //updating the target of the camera and the rotation which leads to the steering of the sphere
    updateCamera(direction, carPosition, keys, target) {
      this.cam.target = target;
        var f = 50;
        if (direction) {
            if (keys[38]) {
                //turn left
                if (keys[37]) {
                    this.cam.alpha = this.cam.alpha + (Math.PI / f);
                }
                //turn right
                if (keys[39]) {
                    this.cam.alpha = this.cam.alpha - (Math.PI / f);
                }
            }
            //moving backwards
            if (keys[40]) {
                if (keys[37]) {
                    this.cam.alpha = this.cam.alpha - (Math.PI / f);
                }
                //turn right
                if (keys[39]) {
                    this.cam.alpha = this.cam.alpha + (Math.PI / f);
                }
            }

        } else {
            this.cam.position = new BABYLON.Vector3(0, 5, 10);
        }
    }
}
