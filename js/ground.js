class Ground{
  constructor(scene){
    const ground = BABYLON.Mesh.CreateGround('ground', 500, 500, 1, scene);

    this._scene = scene;
    this.mesh = ground;
  }

  applyPhysics(){
    this.mesh.physicsImpostor = new BABYLON.PhysicsImpostor(this.mesh, BABYLON.PhysicsImpostor.BoxImpostor, {
      mass: 0,
      restitution: 0.9,
      friction: 0.5,
    }, this._scene);
  }
}
